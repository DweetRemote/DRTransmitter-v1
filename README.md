#########################################################################################################################################
# DRTransmitter-v1													     	|
#	Intended to create a IOT transmission node utilising the dweet.io interface. The application includes			     	|
#       self-installer scripts with configurable parameters such as IOT node name (called thing name in scripts)		     	|
#	and dweet time.			     				     	|
#_______________________________________________________________________________________________________________________________________|
# 																     	|
# This application version does not include:											     	|
#	1. Data Encryption													     	|
#	2. Data capture from serial nodes (ports)										     	|
# It reads data from specified data and ID files and post dweets at configured regular intervals				     	|
#																     	|
# A simple receiver script is also included to give insight on response capture							     	|
#_______________________________________________________________________________________________________________________________________|
#																     	|
#	Author: Syam S Nair													     	|
#	Contributers:														     	|
#	License: Creative Commons, Free BSD											     	|
#	Note: Users please free free to edit and share the code. You are requested						     	|
#	      to keep the spirit of sharing and include the names of all the authors						     	|
#             and contributers name in your script.										     	|
#																     	|
#	Version: 1.0														     	|
#########################################################################################################################################
#########################################################################################################################################


#########################################################################################################################################
#Main Folders:																|
#		|___ Transmitter: Includes the main segements for the transmission							|
#		|															|
#		|___ Receiver: Includes only the core files required for decryption and analysis of data				|
#_______________________________________________________________________________________________________________________________________|
#_______________________________________________________________________________________________________________________________________|
#File Structure:															|
#		|___README.md														|
#		|															|
#		|___transmitter__													|
#		|		 |___config.sh												|
#		|		 |___dweet_data.txt											|
#		|		 |___dweet_ID.txt											|
#		|		 |___info.txt												|
#		|		 |___installer.sh											|
#		|		 |													|
#		|		 |___controller __											|
#		|				  |___controller.sh									|
#		|															|
#		|___Reciever____													|
#				|___config.sh												|
#				|___getdweet.sh												|
#				|___data.txt												|
#########################################################################################################################################
