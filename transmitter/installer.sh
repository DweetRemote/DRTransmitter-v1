#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
# Installer Script for DRTransmitter-v1 for Linux based devices
#----------------------------------------------------------------------------------
#	Author: Syam S Nair
#	Contributers:
#	License: Creative Commons, Free BSD
#	Note: Users please free free to edit and share the code. You are requested
#	      to keep the spirit of sharing and include the names of all the authors
#             and contributers name in your script.
#
#
#	Version: 1.0
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------

#! /bin/bash

WorkingDir=$(pwd);						#path to current working directory
###################################################
#Installer information display and prompt
###################################################
installer_info=$WorkingDir'/info.txt';				
clear;
cat $installer_info;
installer_confirmation="\n\nDo you want to continue with Installation (y/n): ";
echo -ne $installer_confirmation;
read -n 1 prompt_reply;
if [ "$prompt_reply" == "n" ]; then
	echo -e "\n\nQuiting installer";
	exit;
elif [ "$prompt_reply" == "y"  ]; then
	echo -e "\n Installer will continue Installation \n happy dweeting!!!";
else
	echo -e "\n\nInvalid Response. Quitting Installer";
	exit;
fi
###################################################
# Creating Installation folder
###################################################
WorkingRoot='/usr/dweet_remote/'				#path to install scripts

if test -d /usr/dweet_remote; then				#Create root folder if not already created
	echo "dweet_remote folder present";
else
	sudo mkdir /usr/dweet_remote;
fi

###################################################
#Creating Configuration file with custom thing name
###################################################
echo -ne "\n\nPlease enter the connected thing name (Dweet Thing name, unique name should be used), and press [ENTER]: ";
read ThingName;
sudo cp $WorkingDir'/config.sh' $WorkingDir'/tempconfig.sh';
sudo sed -i "s/DefaultDweet/$ThingName/g" $WorkingDir'/tempconfig.sh'; 
sudo mv $WorkingDir'/tempconfig.sh' $WorkingRoot'config.sh';
echo -e "\n IOT thing name configured to: $ThingName \n See $WorkingRoot config.sh for more details"

###################################################
# Creating essential files
###################################################
source $WorkingRoot'config.sh';					#Source the copied configuration file
if [ ! -f $data_file ]; then
	sudo touch $data_file;					#Create dweet data file
fi
if [ ! -f $data_ID ]; then
	sudo touch $data_ID;					#Create dweet ID file
fi

