#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
# Configuration Script for DRTransmitter-v1 for Linux based devices
#
#----------------------------------------------------------------------------------
#	Author: Syam S Nair
#	Contributers:
#	License: Creative Commons, Free BSD
#	Note: Users please free free to edit and share the code. You are requested
#	      to keep the spirit of sharing and include the names of all the authors
#             and contributers name in your script.
#
#
#	Version: 1.0
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------


#! /bin/bash

WorkingRoot='/usr/dweet_remote/';				#path to which script is installed and run from
TempDataPath='/tmp/';						#Temporary data directory path for temporary data dumping
dweetpost_head='https://dweet.io/dweet/for/';			#Common URL for every dweet post
dweet_thing='DefaultDweet';					#IOT thing name - change it to your own thing name

data_file=$WorkingRoot'dweet_data.txt';				#Path to dweet data
data_ID=$WorkingRoot'dweet_dataID.txt';				#Path to dweet ID
dweet_latest=$WorkingRoot'latest_dweet.txt';			#Saves the latest dweet and post time
fail_log=$WorkingRoot'FailLog.txt';				#Path to dweet send fail log
