#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
# Dweet Posting Script for DRTransmitter-v1 for Linux based devices
#
#----------------------------------------------------------------------------------
#	Author: Syam S Nair
#	Contributers:
#	License: Creative Commons, Free BSD
#	Note: Users please free free to edit and share the code. You are requested
#	      to keep the spirit of sharing and include the names of all the authors
#             and contributers name in your script.
#
#
#	Version: 1.0
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------


#! /bin/bash

source /usr/dweet_remote/config.sh				#Source Configuration File
PayloadHead=$dweetpost_head$dweet_thing;			#Configure the payload URL head
dweet_ID=$(cat $data_ID);					#Get Node ID from data ID file
dweet_msgID="?$dweet_ID=";					#Prepare first part of payload
dweet_data=$(cat $data_file);					#Get Node Data from data file (second part of payload)
payload_url=$PayloadHead$dweet_msgID$dweet_data;		#Prepare the URL with payload

tempresponse=$TempDataPath'DweetDump.txt';			#Configuring a Data Dump file
curl $payload_url -s > $tempresponse;				#Sending payload
if grep -Fq "succeeded" $tempresponse; then			#Checking for successful payload delivery
 echo -e "Dweet posted successfully\nDweet info:  " > $dweet_latest
 echo $payload_url >> $dweet_latest				#Successful dweet sent log stored
 echo -e "\n Time of Dweet: " >> $dweet_latest
 date >> $dweet_latest
else								#Logging report incase of failure
 date >> $fail_log;
 echo -e "\nDweet not send. Server response: \n" >> $fail_log;
 cat $tempresponse >> $fail_log;
fi
